options='A a: b: c: f: h i: l: m q: s: t: T:'
help_text="$synopsis print [<options>] [<listing>]

$description - print listings

    -A		Show all. Filters such as \"hide links that I have voted
		on\" will be disabled
    -a <id>  	Show items after <id>. This is Reddits' pagination. See
		https://www.reddit.com/dev/api for more information
    -b <id> 	Show items before <id>. See -a
    -c <when>	When to use colors, never, always or auto
    -f <format>	Format string used for the output
    -i <width>	Indent comments and messages by <width> spaces times the
		nesting depth
    -l <num>	Limit to <num> items
    -m		Mark all messages read when requesting message/* listings
    -q <query>	Query string to search for when using */search listings
    -s <order>	Sort order of the listing. When requesting submissions of
		a subreddit, <order> is one of: hot new rising
		controversial top gilded. For comments one of: confidence
		top new controversial old random qa live
    -T <types>	Comma-delimited list of search result types (sr, link,
		user)
    -t <time>	Timeframe for the top and controversial listings, <time>
		is one of: hour day week month year all
    -h		Print this help and exit

Common listings:
  comments/<id>
  r/<subreddit>/{hot,new,rising,controversial,top,gilded}
  r/<subreddit>/{about,comments}
  user/<yourname>/m/<multiname>/{hot,new,rising,controversial,top,gilded}
  user/<username>
  user/<username>/{about,comments,submitted,gilded}
  user/<username>/{upvoted,downvoted,hidden,saved}
  message/{inbox,unread,messages,comments,selfreply,mentions,sent}
  info/<id1>,<id2>,<id3>...
  by_id/<submission1_id>,<submission2_id>,<submission3_id>..."

##########################################################################

only_once()
{
	only_once() { :; }
	parse_options() { :; }

	logged_in && check_token
	. "$lib_dir"/pretty-time.sh

	# Defaults
	: "${param_c:=auto}"
	: "${param_i:=2}"
	: "${param_l:=99999}"

	$option_c && case $param_c in
		auto|always|never) : ;;
		*) usage "invalid color parameter '$param_c'" ;;
	esac

	$option_i && case $param_i in
		*[!0-9]*) usage "invalid indent parameter '$param_i'" ;;
	esac

	$option_l && case $param_l in
		*[!0-9]*) usage "invalid limit parameter '$param_l'" ;;
	esac

	$option_s && case $param_s in
		hot|new|rising|controversial|top|gilded) : ;;
		*) usage "invalid sort parameter '$param_s'" ;;
	esac

	$option_t && case $param_t in
		hour|day|week|month|year|all) : ;;
		*) usage "invalid time parameter '$param_t'" ;;
	esac

	$option_T && {
		oifs=$IFS IFS=,
		for i in $param_T; do case $i in
			sr|link|user) : ;;
			*) usage "invalid search type '$i'" ;;
		esac; done
		IFS=$oifs
	}

	# Print help after all option parsing is done
	$option_h && usage

	# "-c always" has the highest precedence. With "-c auto", color
	# is disabled when stdout is not a terminal or when the NO_COLOR
	# environment variable has a value
	if
		[ "$param_c" = never ] ||
		[ "$param_c" = auto ] && {
			! [ -t 1 ] || [ -n "$NO_COLOR" ]
		}
	then
		# This variable is used within $format and evaluated
		# shellcheck disable=1091
		no_color=1
	else
		unset no_color
		. "$lib_dir"/color-formats.sh
	fi

	# format is set by (in order of precedence): -f option,
	# REDDIO_FORMAT environment variable, format config variable,
	# default from formats.sh
	if $option_f; then
		format=$param_f
	elif [ -n "${REDDIO_FORMAT:-$format}" ]; then
		format=${REDDIO_FORMAT:-$format}
	else
		. "$lib_dir"/formats.sh
	fi

	_now=$(date +%s)
	num=0
}

cmd_print()
{
	debug "Parsing print sub-command arguments"
	parse_options "$@"
	shift $((OPTIND-1))
	OPTIND=1
	_listing=$1
	debug "$_listing"

	# Only one trailing argument allowed
	[ "$#" -gt 1 ] &&
		usage 'too many arguments'

	only_once

	# Parse trailing argument
	_listing_type=sub
	[ "$_listing" ] && {
		# Remove trailing ?... and &... to prevent attributes
		# in the listing argument
		_listing=${_listing%%[?&]*}

		# Remove leading and trailing slashes
		_listing=${_listing#/}
		_listing=${_listing%/}

		case $_listing in
			morechildren/?*)
				# This is not supposed to be a command
				# shellcheck disable=2209
				_listing_type=more
				link_id=${_listing#*/}
				link_id=${link_id%%/*}
				_children=${_listing##*/}
				_listing=api/morechildren
			;;

			r/?*/about) _listing_type=about_sub ;;
			user/?*/m/?*) _listing_type=sub ;;
			user/?*/about) _listing_type=about_user ;;
			user/?*) _listing_type=user ;;
			comments/?*) _listing_type=comments ;;
			r/?*/comments/?*) _listing_type=comments ;;
			r/?*/comments) _listing_type=sub_comments ;;
			r/?*/search|search|subreddits/search|users/search)
				_listing_type=search
			;;
			r/?*) _listing_type=sub ;;
			message/?*) _listing_type=messages ;;
			by_id/?*) _listing_type=by_id ;;

			info/?*)
				_listing_type=info
				_ids=${_listing#*/}
				_listing=api/info
			;;

			*)
				# TODO: error out on unknown listing type?
				warn "unknown listing type '$_listing'"
				_listing_type=unknown
			;;
		esac

		debug "Listing is of type '$_listing_type'"
	}

	set -- -d raw_json=1 -d threaded=0 ${param_A:+-d show=all} \
		-d "limit=$param_l"

	$option_a && set -- "$@" --data-urlencode "after=$param_a"
	$option_b && set -- "$@" --data-urlencode "before=$param_b"
	$option_s && set -- "$@" --data-urlencode "sort=$param_s"
	$option_t && set -- "$@" --data-urlencode "t=$param_t"
	[ "$_ids" ] && set -- "$@" --data-urlencode "id=$_ids"

	case $_listing_type in
		comments)
			case $_listing in *comments/t3_?*)
				_listing=comments/${_listing#*t3_}
			esac
		;;
		search)
			set -- "$@" \
				--data restrict_sr=1 \
				--data-urlencode "q=$param_q"

			$option_T &&
				set -- "$@" --data "type=$param_T"
		;;
		more)
			set -- "$@" \
				--data api_type=json \
				--data limit_children=false \
				--data "link_id=$link_id" \
				--data "children=$_children"
		;;
	esac

	# Assemble the URL
	_url=${_listing:+$_listing/}

	# TODO: jq error handling
	# reddit api doesn't send json when requesting invalid listings
	{
		query "$_url" --get "$@" ||
			error 1 "failed printing '$_listing'"
	} \
	| sed -e 's%\([^\\]\(\\\\\)*\\\)n%\1r%g' -e 'tx' -e 'b' \
		-e ':x' -e 's%\([^\\]\(\\\\\)*\\\)n%\1r%g' \
	| jq -j --unbuffered -f "$lib_dir"/listings.jq 2>/dev/null \
	| print_formatted "$param_i" $_listing_type "$format" "$option_l"
	_retval=$?

	[ "$_listing_type" = messages ] && $option_m && {
		query api/read_all_messages?raw_json=1 \
			--data-urlencode "filter_types=" >/dev/null
	}

	return $_retval
}

# In this function, we set a lot of variables whichs names are to be used
# in $format. Disable shellcheck warnings about unused variables
# shellcheck disable=2034
print_formatted() {
	_indent=$1
	_type=$2
	_format=$3
	_limit=$4
	shift 4

	# is_mixed is used to conditionally print the link title and
	# subreddit of a comment
	case $_type in
		info|sub_comments|user|messages|search)
			is_mixed=1; unset is_not_mixed
		;;
		*)
			is_not_mixed=1; unset is_mixed
		;;
	esac

	while read -r line; do
		# Unset variables which might be set from
		# previous iterations
		unset -v is_comments is_comment is_user \
			is_t3 is_msg is_sub is_more is_continue

		case $line in
			*$cr*)
				eval "$(printf %s "$line" | tr \\r \\n)"
			;;
			*)
				eval "$line"
			;;
		esac

		case $kind in
			t1) is_comment=1
				# Reddit API is so retardedly inconsistent
				# because comments in "message/..."
				# listings miss link_id As a work-around,
				# we can extract the link_id from
				# "context"
				[ -z "$link_id" ] && {
					link_id=${context#*/comments/}
					link_id=t3_${link_id%%/*}
				}
			;;
			t2)
				is_user=1
			;;
			t3)
				is_t3=1
				# is_comments is used to conditionally
				# print the text of a self-post
				[ "$_type" = comments ] \
				&& is_comments=1
			;;
			t4)
				is_msg=1
				# TODO: json is threaded but has no depth
				# variable
			;;
			t5)
				is_sub=1
			;;
			more)
				if [ "$count" != 0 ]; then
					is_more=1

					# recursively load all comments
					! $_limit && {
						x=morechildren/$link_id
						x=$x/$children
						cmd_print "$x"
						continue
					}
				else
					is_continue=1
				fi
			;;
		esac

		num=$((num+1))

		case $distinguished in
			moderator)
				admin=; moderator=1; special=
				distinguished=M
			;;
			admin)
				admin=1; moderator=; special=
				distinguished=A
			;;
			special)
				admin=; moderator=; special=1
				distinguished=S
			;;
			*)
				admin=; moderator=; special=
				distinguished=
			;;
		esac

		tags=${over18:+[NFSW]}${spoiler:+[Spoiler]}
		tags=$tags${saved:+[Saved]}${archived:+[A]}
		tags=$tags${gilded:+[G]}${locked:+[L]}
		tags=$tags${pinned:+[P]}${stickied:+[S]}

		# Format variables containing an 's' or nothing depending
		# on the respective variable being singular or plural
		case ${num_comments:-1} in
			1) comments_plural= ;;
			*) comments_plural=s ;;
		esac

		case ${score:-1} in
			-1|1) score_plural= ;;
			*) score_plural=s ;;
		esac

		created_time=${created%[.,]*}
		edited_time=${edited%[.,]*}

		created_pretty=$(pretty_time $((_now-created_time)))
		[ -n "${edited:-}" ] &&
			edited_pretty=$(pretty_time $((_now-edited_time)))

		# Print with indentation
		if
			[ -n "$depth" ] &&
			[ "$_indent" -gt 0 ] &&
			[ "$depth" -gt 0 ]
		then
			_pre=$(printf %$((_indent*depth))s)

			eval "printf %s \"$_format\" \
				| sed \"s/^/$_pre/\""
		else
			eval "printf %s \"$_format\""
		fi
	done

	# Return non-zero if no items where printed
	[ $num -gt 0 ]
}
