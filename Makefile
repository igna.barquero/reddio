.POSIX:
.SUFFIXES:

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
DOCDIR=$(PREFIX)/share/doc/reddio
LIBDIR=$(PREFIX)/share/reddio

install:
	mkdir -p -- "$(DESTDIR)$(BINDIR)"
	sed "/^[[:space:]]*lib_dir=/ s|/usr/local/share/reddio|$(LIBDIR)|" \
		reddio >"$(DESTDIR)$(BINDIR)/reddio"
	chmod +x "$(DESTDIR)$(BINDIR)/reddio"

	mkdir -p -- "$(DESTDIR)$(LIBDIR)"
	cp share/reddio/* "$(DESTDIR)$(LIBDIR)/"

	mkdir -p -- "$(DESTDIR)$(DOCDIR)"
	cp doc/* "$(DESTDIR)$(DOCDIR)/"

uninstall:
	rm -- "$(DESTDIR)$(BINDIR)/reddio"

	for f in share/reddio/*; do rm "$(DESTDIR)$(LIBDIR)/$${f##*/}"; done
	rmdir -- "$(DESTDIR)$(LIBDIR)" || :

	for f in doc/*; do rm "$(DESTDIR)$(DOCDIR)/$${f##*/}"; done
	rmdir -- "$(DESTDIR)$(DOCDIR)" || :
